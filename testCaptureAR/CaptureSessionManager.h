//
//  CaptureSessionManager.h
//  testCaptureAR
//
//  Created by Hank  on 2014/8/10.
//  Copyright (c) 2014年 HwangDynChi. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreMedia/CoreMedia.h>
#import <AVFoundation/AVFoundation.h>

@interface CaptureSessionManager : NSObject{
    
}

@property (retain) AVCaptureVideoPreviewLayer *previewLayer;
@property (retain) AVCaptureSession *captureSession;

-(void) addVideoPrevewLayer;
-(void) addVideoInput;

@end
