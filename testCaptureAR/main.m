//
//  main.m
//  testCaptureAR
//
//  Created by Hank  on 2014/8/10.
//  Copyright (c) 2014年 HwangDynChi. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "testCaptureARAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([testCaptureARAppDelegate class]));
    }
}
