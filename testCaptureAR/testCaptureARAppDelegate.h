//
//  testCaptureARAppDelegate.h
//  testCaptureAR
//
//  Created by Hank  on 2014/8/10.
//  Copyright (c) 2014年 HwangDynChi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface testCaptureARAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
