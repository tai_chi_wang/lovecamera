//
//  AROverlayViewController.m
//  testCaptureAR
//
//  Created by Hank  on 2014/8/10.
//  Copyright (c) 2014年 HwangDynChi. All rights reserved.
//

#import "AROverlayViewController.h"

@interface AROverlayViewController ()
@end

@implementation AROverlayViewController

@synthesize captureManager = _captureManager;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    // Do I need to comment it?
    // [super viewDidLoad];
    

}

-(CaptureSessionManager *) captureManager{
    if (!_captureManager) {
        _captureManager = [[CaptureSessionManager alloc] init];
    }
    return _captureManager;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    
    if(interfaceOrientation == UIInterfaceOrientationLandscapeRight)
    {
        [self captureManager].previewLayer.connection.videoOrientation = AVCaptureVideoOrientationLandscapeRight;
    }else if(interfaceOrientation == UIInterfaceOrientationLandscapeLeft)
    {
        [self captureManager].previewLayer.connection.videoOrientation = AVCaptureVideoOrientationLandscapeLeft;
    }
    
    // and so on for other orientations
    
    return ((interfaceOrientation == UIInterfaceOrientationLandscapeRight) || (interfaceOrientation == UIInterfaceOrientationLandscapeLeft));
}

-(void) viewWillAppear:(BOOL)animated{
    
    // Do any additional setup after loading the view.
    //[self setCaptureManager:[[CaptureSessionManager alloc] init]];
    [[self captureManager] addVideoInput];
    [[self captureManager] addVideoPrevewLayer];
    //CGRect layerRect = [[[self view] layer] bounds];
    CGRect layerRect = [UIScreen mainScreen].bounds;
    //CGRect layerRect = CGRectMake(0, 0, 480, 320);
    [[[self captureManager] previewLayer] setBounds:layerRect];
    [[[self captureManager] previewLayer] setPosition:CGPointMake(CGRectGetMidX(layerRect), CGRectGetMidY(layerRect))];
    
    [[[self view] layer] addSublayer: [[self captureManager] previewLayer]];
    
    // augmented views
    UIImageView *overlayImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"overlayHeartShape.png"]];
    [overlayImageView setFrame:CGRectMake(30, 100, 260, 200)];
    [[self view] addSubview:overlayImageView];
    
    UIButton *overlayButton = [UIButton buttonWithType:UIButtonTypeSystem];
    [overlayButton setFrame:CGRectMake(130, 320, 60, 30)];
    [overlayButton addTarget:self action:@selector(shutButtonPressed) forControlEvents:UIControlEventTouchUpInside];
    [[self view] addSubview:overlayButton];
    
    UILabel *tempLabel = [[UILabel alloc] initWithFrame:CGRectMake(100, 50, 120, 30)];
    [self setHeartLabel:tempLabel];
    
    [self.heartLabel setBackgroundColor:[UIColor clearColor]];
    [self.heartLabel setFont:[UIFont fontWithName:@"Courier" size:18.0]];
    [self.heartLabel setTextColor:[UIColor redColor]];
    [self.heartLabel setText:@"Heart label"];
    
    [self.heartLabel setHidden:YES];
    [[self view] addSubview:self.heartLabel];
    
    [[self.captureManager captureSession] startRunning];
}

-(void) shutButtonPressed{
    [[self heartLabel] setHidden:NO];
    [self performSelector:@selector(hideLabel:) withObject:self.heartLabel afterDelay:2];
}

-(void) hideLabel: (UILabel *)label{
    [label setHidden:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
