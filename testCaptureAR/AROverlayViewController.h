//
//  AROverlayViewController.h
//  testCaptureAR
//
//  Created by Hank  on 2014/8/10.
//  Copyright (c) 2014年 HwangDynChi. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CaptureSessionManager.h"

@interface AROverlayViewController : UIViewController{
    
}

@property (nonatomic, strong) CaptureSessionManager *captureManager;
@property (nonatomic, retain) UILabel *heartLabel;

@end
