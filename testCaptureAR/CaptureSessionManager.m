//
//  CaptureSessionManager.m
//  testCaptureAR
//
//  Created by Hank  on 2014/8/10.
//  Copyright (c) 2014年 HwangDynChi. All rights reserved.
//

#import "CaptureSessionManager.h"

@implementation CaptureSessionManager

#pragma mark Capture Session Configuration

-(id) init{
    if ((self = [super init])) {
        [self setCaptureSession:[[AVCaptureSession alloc] init]];
    }
    return self;
}

-(void) addVideoPrevewLayer{
    [self setPreviewLayer:[[AVCaptureVideoPreviewLayer alloc] initWithSession:[self captureSession]]];
    [[self previewLayer] setVideoGravity:AVLayerVideoGravityResizeAspectFill];
    
    CGRect screenRect = [UIScreen mainScreen].bounds;
    self.previewLayer.frame = screenRect;
    //self.previewLayer.orientation = AVCaptureVideoOrientationPortrait;
    self.previewLayer.connection.videoOrientation = [[UIDevice currentDevice] orientation];
    
}

-(void) dealloc {
    [[self captureSession] stopRunning];
}

-(void) addVideoInput{
    AVCaptureDevice *videoDevice = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    if (videoDevice) {
        NSError *error;
        AVCaptureDeviceInput *videoIn = [AVCaptureDeviceInput deviceInputWithDevice:videoDevice error:&error];
        if (!error) {
            if ([[self captureSession] canAddInput:videoIn]) {
                [[self captureSession] addInput:videoIn];
            }else{
                NSLog(@"Couldn't add video input");
            }
        }else{
            NSLog(@"Couldn't create video input");
        }
    }else{
        NSLog(@"Couldn't create vide device");
    }
}

@end
